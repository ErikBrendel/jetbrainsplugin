
public class CombinedSafeAction extends TextChangeAction {

    public CombinedSafeAction() {
        super("Combined Cleanup");
    }

    protected String manipulateContent(String originalContent) {
        return
                OrganizeIncludesAction.organizeIncludes(
                FixLicenseHeaderAction.fixHeaders(
                    originalContent
                ));
    }
}
