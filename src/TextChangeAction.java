import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleUtilCore;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;

public abstract class TextChangeAction extends AnAction {

    public TextChangeAction(String name) {
        super(name);
    }

    @Override
    public void actionPerformed(AnActionEvent anActionEvent) {
        Project project = anActionEvent.getRequiredData(PlatformDataKeys.PROJECT);
        Editor editor = anActionEvent.getData(PlatformDataKeys.EDITOR);
        if (editor == null) return;
        Document currentOpenDocument = editor.getDocument();
        VirtualFile currentOpenFile = FileDocumentManager.getInstance().getFile(currentOpenDocument);
        if (currentOpenFile == null) return;
        Statics.currentOpenPath = currentOpenFile.getPath().substring(project.getBasePath().length() + "/src/".length());

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                String originalContent = currentOpenDocument.getText();
                currentOpenDocument.setText(manipulateContent(originalContent));
            }
        };
        WriteCommandAction.runWriteCommandAction(project, runnable);
    }

    protected abstract String manipulateContent(String originalContent);
}
