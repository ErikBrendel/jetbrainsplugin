import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FixLicenseHeaderAction extends TextChangeAction {

    public static final String HEADER = "" +
            "/******************************************************************************\n" +
            " __  _  __ _ _____ __  _    __ __  __   __ _____ ___ ___ ___ _ ___ ______  __\n" +
            "| _\\| |/ _| |_   _/  \\| |  |  V  |/  \\/' _|_   _| __| _ | _,| | __/ _| __/' _/\n" +
            "| v | | [/| | | || /\\ | |_ | \\_/ | /\\ `._`. | | | _|| v | v_| | _| \\_| _|`._`.\n" +
            "|__/|_|\\__|_| |_||_||_|___||_| |_|_||_|___/ |_| |___|_|_|_| |_|___\\__|___|___/\n" +
            "\n" +
            " Copyright (C) __YEAR__ Digital Masterpieces GmbH, All rights reserved\n" +
            " http://www.digitalmasterpieces.com/\n" +
            "\n" +
            " ******************************************************************************/\n";

    public static final String HEADER_START = "/************************";
    public static final String HEADER_END = "************************/\n";
    public static final Pattern YEAR_REGEX = Pattern.compile("Copyright \\(C\\) (\\d{4})(?:|-\\d{4}) Digital Masterpieces GmbH");
    private static final int CURRENT_YEAR = 2018;


    public FixLicenseHeaderAction() {
        super("Adjust License Header");
    }

    /**
     * take a files text content and adjust the license header
     * @param originalContent the complete file as string
     * @return the adjusted file content
     */
    protected String manipulateContent(String originalContent) {
        return fixHeaders(originalContent);
    }

    public static String fixHeaders(String originalContent) {
        boolean hasHeader = originalContent.startsWith(HEADER_START) && originalContent.contains(HEADER_END);

        if (!hasHeader) {
            return getHeader() + originalContent;
        } else {
            String[] split = originalContent.split(Pattern.quote(HEADER_END));
            int minYear = getMinYear(split[0]);
            String yearString = "" + minYear;
            if (minYear < CURRENT_YEAR) {
                yearString += "-" + CURRENT_YEAR;
            }
            return getHeader(yearString) + split[1];
        }
    }

    private static String getHeader() {
        return getHeader("" + CURRENT_YEAR);
    }

    private static String getHeader(String yearString) {
        return HEADER.replace("__YEAR__", yearString);
    }

    private static int getMinYear(String header) {
        Matcher m = YEAR_REGEX.matcher(header);

        if (m.find()) {
            return Integer.parseInt(m.group(1));
        } else {
            return CURRENT_YEAR;
        }
    }
}
