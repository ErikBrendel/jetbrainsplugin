
public class CombinedExperimentalAction extends TextChangeAction {

    public CombinedExperimentalAction() {
        super("Combined Cleanup");
    }

    protected String manipulateContent(String originalContent) {
        return
                OrganizeIncludesAction.organizeIncludes(
                RemoveUnusedIncludesAction.removeUnusedIncludes(
                FixLicenseHeaderAction.fixHeaders(
                    originalContent
                )));
    }
}
