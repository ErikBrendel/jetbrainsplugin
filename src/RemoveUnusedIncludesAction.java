import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RemoveUnusedIncludesAction extends TextChangeAction {

    private static final String INCLUDE_LINE_PREFIX = "#include ";
    private static final Map<String, String> CLASS_REPLACE = new HashMap<>();

    static {
        CLASS_REPLACE.put("QDebug", "qDebug");
        CLASS_REPLACE.put("QWarning", "qWarning");
    }

    public RemoveUnusedIncludesAction() {
        super("Remove Unused Includes");
    }

    /**
     * take a files text content and adjust the includes
     * @param originalContent the complete file as string
     * @return the adjusted file content
     */
    protected String manipulateContent(String originalContent) {
        return removeUnusedIncludes(originalContent);
    }

    public static String removeUnusedIncludes(String originalContent) {
        String[] lines = originalContent.split("\n");

        List<String> result = new ArrayList<>();
        for (String line : lines) {
            if (line.startsWith(INCLUDE_LINE_PREFIX)) {

                String fullIncludeText = line.substring(INCLUDE_LINE_PREFIX.length() + 1);
                fullIncludeText = fullIncludeText.substring(0, fullIncludeText.length() - 1);

                String[] path = fullIncludeText.split("/");
                String includedFile = path[path.length - 1].split("\\.")[0];

                if (includeIsUsed(includedFile, originalContent)) {
                    result.add(line);
                }
            } else {
                result.add(line);
            }
        }
        
        return String.join("\n", result) + "\n";
    }

    private static boolean includeIsUsed(String fileName, String usageArea) {
        String className = fileToClass(fileName);
        Matcher countMatcher = Pattern.compile("\\b" + className + "\\b").matcher(usageArea);

        int count = 0;
        while (countMatcher.find()) {
            count++;
        }

        if (className.equals(fileName)) {
            count--; //the one match in the include
        }
        return count > 0;
    }

    private static String fileToClass(String fileName) {
        if (fileName.startsWith("ui_")) {
            return "Ui::" + fileName.substring("ui_".length());
        }
        return CLASS_REPLACE.getOrDefault(fileName, fileName);
    }
}
