import java.util.*;

public class OrganizeIncludesAction extends TextChangeAction {

    private static final String INCLUDE_LINE_PREFIX = "#include ";
    private static final String UNNAMED_GROUP_PREFIX = "<UNNAMED>_[GROUP]_(TROLOLOL)_/XD\\_ROFL_NOBODY_WILL_GUESS_THAT: ";

    public OrganizeIncludesAction() {
        super("Organize Includes");
    }

    /**
     * take a files text content and adjust the includes
     * @param originalContent the complete file as string
     * @return the adjusted file content
     */
    protected String manipulateContent(String originalContent) {
        return organizeIncludes(originalContent);
    }

    public static String organizeIncludes(String originalContent) {
        String[] lines = originalContent.split("\n");


        //find the area of the include block. Collect every include statement
        int firstIncludeLine = -1;
        int lastIncludeLine = -1;
        List<String> includes = new ArrayList<>();
        boolean includeBlockEnded = false;
        for (int l = 0; l < lines.length; l++) {
            String line = lines[l];
            if (line.startsWith(INCLUDE_LINE_PREFIX) && !isInlLine(line)) {
                if (firstIncludeLine < 0) {
                    firstIncludeLine = l;
                }
                if (!includeBlockEnded) {
                    lastIncludeLine = l;
                }
                includes.add(line.substring(INCLUDE_LINE_PREFIX.length()));
            } else if (line.trim().startsWith("//") || line.trim().isEmpty()) {
                //ignore comments and space
            } else {
                if (firstIncludeLine >= 0) {
                    includeBlockEnded = true;
                }
            }
        }
        if (firstIncludeLine < 0) { //no include statement whatsoever
            return originalContent;
        }

        //dilate include block around leading comments and leading and trailing space
        while (firstIncludeLine > 0 && lines[firstIncludeLine - 1].startsWith("//")) {
            firstIncludeLine--;
        }
        while (firstIncludeLine > 0 && lines[firstIncludeLine - 1].trim().isEmpty()) {
            firstIncludeLine--;
        }
        while (lastIncludeLine < lines.length - 1 && lines[lastIncludeLine + 1].trim().isEmpty()) {
            lastIncludeLine++;
        }

        List<String> newIncludeBlock = generateIncludeBlock(includes);

        List<String> result = new ArrayList<>();
        result.addAll(Arrays.asList(lines).subList(0, firstIncludeLine));
        result.add("");
        result.addAll(newIncludeBlock);
        result.add("");
        for (int l = lastIncludeLine + 1; l < lines.length; l++) {
            String line = lines[l];
            if (!line.startsWith(INCLUDE_LINE_PREFIX) || isInlLine(line)) {
                result.add(line);
            }
        }
        return String.join("\n", result) + "\n";
    }

    private static boolean isInlLine(String line) {
        return line.trim().endsWith(".inl\"");
    }

    private static List<String> generateIncludeBlock(List<String> rawIncludes) {
        List<String> strippedIncludes = new ArrayList<>(rawIncludes.size());
        for (String include : rawIncludes) {
            if ((include.startsWith("<") && include.endsWith(">")) || (include.startsWith("\"") && include.endsWith("\""))) {
                strippedIncludes.add(include.substring(1, include.length() - 1));
            }
        }

        for (int i = 0; i < strippedIncludes.size(); i++) {
            String include = strippedIncludes.get(i);

            //remove "src/" everywhere
            if (include.startsWith("src/")) {
                include = include.substring("src/".length());
            }

            //relativate qt includes
            if (include.matches("Q\\w+/Q\\w+")) {
                include = include.split("/")[1];
            }

            strippedIncludes.set(i, include);
        }


        //add the includes to groups
        Map<String, Set<String>> groupedIncludes = new TreeMap<>();
        for (String include : strippedIncludes) {
            String groupName = getGroupNameForInclude(include);
            if (!groupedIncludes.containsKey(groupName)) {
                groupedIncludes.put(groupName, new TreeSet<>(String.CASE_INSENSITIVE_ORDER));
            }
            groupedIncludes.get(groupName).add(include);
        }


        //print the groups
        List<String> lines = new ArrayList<>();
        boolean firstInclude = true;
        for (Map.Entry<String, Set<String>> group : groupedIncludes.entrySet()) {
            if (firstInclude) {
                firstInclude = false;
            } else {
                lines.add("");
            }
            String groupName = group.getKey();
            if (!groupName.startsWith(UNNAMED_GROUP_PREFIX)) {
                lines.add("// " + groupName.toUpperCase());
            }
            for (String include : group.getValue()) {
                String includeLine = INCLUDE_LINE_PREFIX;
                includeLine += getIncludeStartChar(groupName);
                includeLine += include;
                includeLine += getIncludeEndChar(groupName);
                lines.add(includeLine);
            }
        }

        return lines;
    }

    private static String getGroupNameForInclude(String include) {
        if (include.startsWith("ui_")) {
            //qt ui files, should be separated
            return UNNAMED_GROUP_PREFIX + "B";

        } else if (include.endsWith(".h")) {
            //own header files
            String includePath = include;
            if (!includePath.contains("/")) {

                //check for "just myself"
                String[] slashSplit = Statics.currentOpenPath.split("/");
                String myFileName = slashSplit[slashSplit.length - 1].split("\\.")[0];
                if (myFileName.equals(include.split("\\.")[0])) {
                    //just the same file name as me, with other ending
                    return UNNAMED_GROUP_PREFIX + "A";
                }

                //use the current location as include path
                includePath = Statics.currentOpenPath;
            }

            String[] pathSteps = includePath.split("/");
            if ((pathSteps[0].equals("core") || pathSteps[0].equals("apps")) && pathSteps.length > 2) {
                return pathSteps[1];
            } else {
                return pathSteps[0];
            }

        } else {
            //library includes
            if ((include.startsWith("Q") && Character.isUpperCase(include.charAt(1)))
                    || (include.startsWith("Qt") && Character.isUpperCase(include.charAt(2)))) {
                return "qt";
            }
            if (include.contains("/")) {
                return include.split("/")[0];
            }
            return "std";
        }
    }

    private static String getIncludeStartChar(String groupName) {
        return usesProjectIncludeSyntax(groupName) ? "\"" : "<";
    }

    private static String getIncludeEndChar(String groupName) {
        return usesProjectIncludeSyntax(groupName) ? "\"" : ">";
    }

    private static boolean usesProjectIncludeSyntax(String groupName) {
        return !(groupName.equals("qt") || groupName.equals("std"));
    }
}
